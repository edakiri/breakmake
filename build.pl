/* You can use, redistribute, or modify this software under the terms of the
GNU Affero General Public License as published by the Free Software Foundation,
either version 3 of the License, or any later version.
*/

#!/usr/bin/swipl -s
/*build something trivial*/

:- use_module(library(dcg/basics)). % Provides string predicate needed for use with phrase predicate

% http://www.swi-prolog.org/pldoc/man?section=apply
:- use_module(library(apply)). % for maplist


%	Section : Build Dependencies

source("trivial",["trivial.c","value.c"]) .


%	Section : Build Environment

% only 1 value at a time is supported
compiler('/usr/bin/gcc') .
%compiler('/bin/echo') .

% Any combination of compile tasks is valid.
:- dynamic compileTask(print) .
compileTask(print) .
:- dynamic compileTask(compile) .
compileTask(compile) .

% Either whole or parts
%sourceToObject(whole) .
sourceToObject(parts) .

%	Section: Begin of build tool

built :- built("trivial")
	.
built(B) :- % Find solution for building a file, building it in the process
	exists_file(B)
	;
	sourceToObject(SourceToObject) ,
	buildAs(SourceToObject,B)
	.
buildAs(parts,B) :- % build process uses a series of intermediate compilation of parts. .o for .c files
	source(B,Source) ,
	buildPart(Source) ,
	intermediaries(Source,Intermediaries) ,
	Arguments=['--lto','-o',B|Intermediaries] ,
	compile(Arguments)
	.
buildAs(whole,B) :- % compilation where source files go into the final output. cc -o OutPut *.c
	source(B,Source) ,
	Arguments=['--whole-program','-o',B|Source] ,
	compiler(Executable) ,
	catch(process_create(Executable,Arguments,[]),E,errorHandle(E))
	.
/* compile many parts. Perhaps should bind variable to output file name in second parameter. */
buildPart([]) :-
	true
	.
buildPart(B) :-
	[P|T]=B ,
	buildPart(T) ,
	intermediary(P,O) ,
	format("File to test: ~s~n",[O]),
	(
		exists_file(O)
		;
		Arguments=['--lto','-c',P] ,
		compile(Arguments)
	)
	.
compile(Arguments) :-
	forall(compileTask(T),compileTaskDo(T,Arguments))
	.
compileTaskDo(print,Arguments) :-
	compiler(Executable) ,
	write(Executable) ,
	maplist(format(' ~s'),Arguments) , % FixMe
	nl
	.
compileTaskDo(compile,Arguments) :-
	compiler(Executable) ,
	catch(process_create(Executable,Arguments,[]),E,errorHandle(E))
	.
errorHandle(H) :-
	error(C,_)=H ,
	(
		existence_error(_,Err)=C ,
		write_term(user_error,'Command not found: ',[ignore_ops(true)])
		;
		process_error(_,exit(Err))=C ,
		write_term(user_error,'Error code returned: ',[ignore_ops(true)])
		;
		write_term(user_error,'Unknown exception: ',[ignore_ops(true)]) ,
		write_term(user_error,C,[ignore_ops(true)]) ,
		nl ,
		fail
	) ,
	nl ,
	write_term(user_error,Err,[ignore_ops(true)]) ,
	nl
	.
intermediaries([],[]) :- true .

/* For input of .c source files, what are the .o intermediary files.
Works in reverse also. Is the reverse less efficient?*/
intermediaries(Source,Intermediary) :-
	[S|RemainderSource]=Source ,
	intermediaries(RemainderSource,RemainderIntermediary) ,
	intermediary(S,I) ,
	[I|RemainderIntermediary]=Intermediary
	.
intermediary(Source,Intermediary) :-
	phrase(inPutFileName(BasisName),Source) ,
	append(BasisName,".o",Intermediary)
	.
inPutFileName(BasisName) --> string(BasisName) , ".c" .
% call with phrase(inPutFileName(Prefix),"FileName.Extension").
